<?php

/**
 * Implements hook_menu().
 *
 * @return array
 */
function good_relations_tags_menu() {
  $items = array();
  // Item for viewing available Tags
  $items['admin/config/user-interface/view_good_relations_tags'] = array(
    'title' => 'Good relations tags',
    'description' => 'Add new and View all good relations tags in the database',
    'page callback' => 'good_relations_tags_view_tags',
    'access arguments' => array('user_is_admin'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

//----------------------- NEW TAG FORM -----------------------------------------

/**
 * @param $form
 * @param $form_state
 * @return mixed
 */
function good_relations_tags_new_tag($form, &$form_state) {
  $form['grt_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('The name of the tag'),
    '#description' => t('Please insert a valid tag name'),
    '#size' => 64,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['grt_description'] = array(
    '#type' => 'textfield',
    '#title' => t('The description of the tag'),
    '#description' => t('What is it used for ?'),
    '#size' => 128,
    '#maxlenght' => 256,
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add tag'),
  );

  return $form;
}

/**
 * Implements validation handler for the above form.
 */
function good_relations_tags_new_tag_validate($form, &$form_state) {
  $value = $form_state['values']['grt_tag'];
  // Already exists in the database.
  $tags = _tags_get_all_tags();
  foreach ($tags as $tag) {
    if ($tag->grt_tag == $value) {
      form_set_error('', t($value . ' already exists in the database'));
    }
  }

  // GR specifics.
  if (strpos($value, 'gr:') !== 0) {
    form_set_error('', t('The tag must contain and start with gr: (good relations)'));
  }
}

/**
 * Implements submit button for the above form.
 * Insert the data into the db table.
 */
function good_relations_tags_new_tag_submit($form, &$form_state) {
  $grt_id = db_insert('good_relations_tags')
      ->fields(array(
        'grt_tag' => $form_state['values']['grt_tag'],
        'grt_description' => $form_state['values']['grt_description'],
      ))
      ->execute();
  drupal_set_message('Your tag has been successfully added');
}

//------------------------- VIEW TAGS FORM -------------------------------------

/**
 * @return string
 */
function good_relations_tags_view_tags() {
  $tags = _tags_get_all_tags();

  $header = array(t('ID'), t('Tag Name'), t('Tag Description'));
  $rows = array();

  foreach ($tags as $tag) {
    $rows[] = array(
      $tag->grt_id,
      $tag->grt_tag,
      $tag->grt_description,
    );
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $form = drupal_get_form('good_relations_tags_new_tag');
  $output .= drupal_render($form);
  return $output;
}

/**
 * @return DatabaseStatementInterface|null
 */
function _tags_get_all_tags() {
  $query = db_select('good_relations_tags', 'grt');
  $query
      ->fields('grt', array('grt_id', 'grt_tag', 'grt_description'))
      ->orderBy('grt.grt_id');
  $tags = $query->execute();

  return $tags;
}
