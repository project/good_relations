<?php

/**
 * Class good_relations_regular_content_types
 */
class good_relations_content_types implements good_relations_interface {

  protected $type;
  protected $bundle;
  protected $mapping_fields = array();

  function __construct($type = NULL, $bundle = NULL, $mapping_fields = NULL) {
    //revise here when implementing entity support
    $this->type = 'node';
    $this->bundle = $bundle;
    $this->mapping_fields = $mapping_fields;
  }

  // Extra (Getters and Setters).
  public function setType($type = NULL) {
    $this->type = $type;
  }

  public function getType() {
    return $this->type;
  }

  public function setBundle($bundle = NULL) {
    $this->bundle = $bundle;
  }

  public function getBundle() {
    return $this->bundle;
  }

  public function setMappingFields($mapping_fields = NULL) {
    $this->mapping_fields = $mapping_fields;
  }

  public function getMappingFields() {
    return $this->mapping_fields;
  }

  //---------------------------------------------------------------------------

  public function resultMessage() {
    return t('The following ' . $this->type . ' type has been mapped: ' . $this->bundle);
  }

  /**
   * @return array
   */
  public function namespaces() {
    return array(
      'gr' => 'http://purl.org/goodrelations/v1#'
    );
  }

  /**
   * The RDF MAPPING! (node)
   *
   * @return array
   */
  public function map() {
    $rdf_mapping = array();

    // Good Relations offering set up.
    $rdf_mapping = array_merge($rdf_mapping, array('rdftype' => array('gr:Offering')));
    // The title does not appear a normal field.
    $rdf_mapping = array_merge($rdf_mapping, array('title' => array('predicates' => array('gr:name'))));
    foreach ($this->mapping_fields as $id => $value) {
      $rdf_mapping = array_merge($rdf_mapping, array($id => array('predicates' => array($value))));
    }

    $product_mapping = array(
      'type' => $this->type,
      'bundle' => $this->bundle,
      'mapping' => $rdf_mapping,
    );

    return $product_mapping;
  }

}
